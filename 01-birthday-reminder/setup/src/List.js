import React from 'react';

const List = ({people}) => {
  return (
    <>
      <h2>{people.map((person)=>{
        const {id,name,age,image} = person;
        return(
          <article className='person' key={id}>
            <img src={image} alt={name} />
            <div>
              <h4>{name}</h4>
              <h4>{age} years</h4>
            </div>
          </article>
        )
      })}</h2>
    </>
  );
};

export default List;
